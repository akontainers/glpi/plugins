FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c as builder
# hadolint ignore=DL3018,DL3019
RUN set -eux; \
        apk add alpine-sdk sudo; \
        abuild-keygen -a -i -n;
COPY / /abuild
WORKDIR /abuild
RUN set -eux; \
        abuild -F -r -P /abuild; \
        find . -name '*.apk' -type f -exec apk add --no-cache {} \;;

FROM scratch
COPY --from=builder /var/www/html/plugins/ /var/www/html/plugins
